from rest_framework import generics
from chequeados.models import Articulo
from .serializers import ArticuloSerializer

from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

#from django.core import serializers

@api_view(['GET'])
def articulos_list(request, pk):
    articulos = Articulo.objects.all().filter(category_id=pk)

    if request.method == 'GET':
    	limit = request.GET.get('limit')
    	page = request.GET.get('page')

    	paginator = Paginator(articulos, limit)

    	try:
    		articulos = paginator.page(page)
    	except PageNotAnInteger:
    		articulos = paginator.page(1)
    	except EmptyPage:
    		articulos = ''

    	if paginator.page(page).has_next():
    		next = "http://local.hack.io/chequeados/api/categoria/"+pk+"/?limit="+limit+"&page="+str(paginator.page(page).next_page_number())
    	else:
    		next = None

    	if paginator.page(page).has_previous():
    		previous = "http://local.hack.io/chequeados/api/categoria/"+pk+"/?limit="+limit+"&page="+str(paginator.page(page).previous_page_number())
    	else:
    		previous = None

    	serializer = ArticuloSerializer(articulos, many=True)

    	return Response({
            'count': paginator.count,
    		'next': next,
    		'previous': previous,
            'results': serializer.data
        })

    	
class ArticuloListView(generics.ListAPIView):
	queryset = Articulo.objects.all()
	serializer_class = ArticuloSerializer

class ArticuloDetailView(generics.RetrieveAPIView):
	queryset = Articulo.objects.all()
	serializer_class = ArticuloSerializer