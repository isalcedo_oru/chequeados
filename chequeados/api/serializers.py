from rest_framework import serializers
from chequeados.models import Articulo
from chequeados.models import Empresa
from chequeados.models import Categoria

class EmpresaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Empresa
		fields = ('id', 'title', 'slug', 'avatar')

class CategoriaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Categoria
		fields = ('id', 'title', 'slug')

class ArticuloSerializer(serializers.ModelSerializer):
	author   = EmpresaSerializer(read_only=True)
	category = CategoriaSerializer(read_only=True)
	class Meta:
		model = Articulo
		fields = ('id', 'title', 'slug', 'intro_text', 'body', 'author', 'category', 'likes')