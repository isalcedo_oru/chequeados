from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^chequeados/$',
		views.ArticuloListView.as_view(),
		name='chequeados_lista'),
	
	url(r'^chequeados/(?P<pk>\d+)/$',
		views.ArticuloListView.as_view(),
		name='chequeados_lista'),

	url(r'^categoria/(?P<pk>[0-9]+)/$',
		views.articulos_list,
		name='categoria_lista'),
]
