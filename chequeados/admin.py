from django.contrib import admin

# Register your models here.

from .models import Articulo
from .models import Categoria
from .models import Empresa

class ArticuloAdmin(admin.ModelAdmin):
	list_display = ('title', 'slug', 'author', 'publish','status')
	list_filter = ('status', 'created', 'publish', 'author')
	search_fields = ('title', 'body')
	prepopulated_fields = {'slug': ('title',)}
	raw_id_fields = ('author',)
	date_hierarchy = 'publish'
	ordering = ['status', 'publish']
	class Media:
		js = ('/static/js/tinymce/tinymce.min.js', '/static/js/tinymce/tinymce.conf.js')

class CategoriaAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('title',)}

class EmpresaAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('title',)}

admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Empresa, EmpresaAdmin)
