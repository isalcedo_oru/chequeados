from django.conf.urls import url
from django.conf.urls import include

from . import views
urlpatterns = [
	# post views
	url(r'^$', views.articulo_list, name='articulo_list'),
	url(r'^chequeado/(?P<articulo>[-\w]+)/$', views.articulo_detail, name='articulo_detail'),
	url(r'^categoria/(?P<categoria>[-\w]+)/$', views.categoria_list, name='categoria_list'),
	url(r'^api/', include('chequeados.api.urls', namespace='api')),
	# url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/'\
	# 	r'(?P<post>[-\w]+)/$',
	# 	views.post_detail,
	# 	name='post_detail')
]