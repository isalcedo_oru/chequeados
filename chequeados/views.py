from django.shortcuts import render
from django.core import serializers

# Create your views here.
from django.shortcuts import render, get_object_or_404
from chequeados.models import Articulo
from chequeados.models import Empresa
from chequeados.models import Categoria
def articulo_list(request):
	categorias       = Categoria.objects.all()
	articulos        = Articulo.objects.all()[:4]
	articulos_schema = Articulo.objects.all() #Para MicroData Schema.org
	return render(request,
		'chequeados/articulo/list.html',
		{
			'categorias': categorias,
			'articulos': articulos,
			'articulos_schema': articulos_schema
		})

def categoria_list(request, categoria):
	categoria_actual = Categoria.objects.get(slug__exact=categoria)
	categorias       = Categoria.objects.all()
	articulos        = Articulo.objects.all().filter(category_id=categoria_actual.id)[:4]
	articulos_schema = Articulo.objects.all().filter(category_id=categoria_actual.id) #Para MicroData Schema.org
	return render(request,
		'chequeados/categoria/list.html',
		{
			'categoria_actual': categoria_actual,
			'categorias': categorias,
			'articulos': articulos,
			'articulos_schema': articulos_schema
		})

def articulo_detail(request, articulo):
#Dinámico
	articulo   = get_object_or_404(Articulo, slug=articulo, status='published')
	empresa    = get_object_or_404(Empresa, pk=articulo.author_id)
	categoria  = get_object_or_404(Categoria, pk=articulo.category_id)
	return render(request,
		'chequeados/articulo/article.html',
		{
			'chequeado': articulo,
			'empresa': empresa,
			'categoria': categoria,
		})