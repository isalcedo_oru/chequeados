from django.db import models
from django.conf import settings

# Create your models here.
from django.utils import timezone
from django.contrib.auth.models import User

class Categoria(models.Model):
	title    = models.CharField(max_length=200, db_index=True)
	slug     = models.SlugField(max_length=200, db_index=True, unique=True)
	main_img = models.ImageField(upload_to='categorias/',max_length=255)

	class Meta:
		ordering = ('title',)
		verbose_name = 'categoría'
		verbose_name_plural = 'categorías'
	
	def __str__(self):
		return self.title

class Empresa(models.Model):
	title             = models.CharField(max_length=200, db_index=True)
	slug              = models.SlugField(max_length=200, db_index=True, unique=True)
	small_description = models.TextField()
	avatar            = models.ImageField(upload_to='empresas/',max_length=255)
	avatar_white     = models.ImageField(upload_to='empresas/',max_length=255, null=True)

	class Meta:
		ordering = ('title',)
		verbose_name = 'empresa'
		verbose_name_plural = 'empresas'
	
	def __str__(self):
		return self.title

class Articulo(models.Model):
	STATUS_CHOICES = (
		('draft', 'Draft'),
		('published', 'Published'),
		)

	title      = models.CharField(max_length=250)
	slug       = models.SlugField(max_length=250, unique=True)
	author     = models.ForeignKey(Empresa, related_name='chequeado_empresa')
	category   = models.ForeignKey(Categoria, related_name='chequeado_categoria', default=0)
	main_img   = models.ImageField(upload_to='subidas/%m-%Y/',max_length=255)
	intro_text = models.TextField(null=True)
	body       = models.TextField()
	likes      = models.IntegerField(default=0)
	publish    = models.DateTimeField(default=timezone.now)
	created    = models.DateTimeField(auto_now_add=True)
	updated    = models.DateTimeField(auto_now=True)
	status     = models.CharField(max_length=10,
		choices=STATUS_CHOICES,
		default='draft')

	class Meta:
		ordering = ('-publish',)
		def __str__(self):
			return self.title

	@models.permalink
	def get_absolute_url(self):
		return reverse('chequeados:chequeado',
			args=[self.slug])