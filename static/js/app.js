$(document).foundation();
$(document).ready(function(){

	var ajaxNextPagination = 0; //Variable que contendrá los valores de la URL siguiente en la paginación AJAX.

/* No era SEO
	if ($('#articulo-template').length) {
		$.get( "/chequeados/api/chequeados/?format=json", function( data ) {
			ajaxNextPagination = data.next;
			var articulos = data.results;
			var TemplateFinalScript = $("#articulo-template").html();
			var TemplateFinal = Handlebars.compile(TemplateFinalScript);
			var context = articulos;
			var CompiledFinalHtml = TemplateFinal(context);
			$('div.posts .posts-handle').prepend(CompiledFinalHtml);	
		});
	}

	if ($('#categoria-template').length) {
		var TemplateFinalScript = $("#categoria-template").html();
		var context = categorias;
		var TemplateFinal = Handlebars.compile(TemplateFinalScript);
		var CompiledFinalHtml = TemplateFinal(context);
		$('div.posts .categories-handle .c-list').append(CompiledFinalHtml);
	}
*/
	enquire.register("screen and (max-width: 63.9375em)", function() { //Small
		var currently = false;
		var win = $(window);
		$(document).on("scroll", function () {
			if (typeof(cat_principal) !== 'undefined'){
				if(!currently && $(document).height() - win.height() == win.scrollTop() && (ajaxNextPagination == 0 || ajaxNextPagination != null) ) {
					currently = true;
					if (ajaxNextPagination == 0 && typeof(page) !== 'undefined') {
						next = "/chequeados/api/categoria/"+cat_principal+"/?format=json&limit=4&page="+page;
					} else {
						next = "";
					}
					if (ajaxNextPagination != 0 && ajaxNextPagination != null ) {
						next = ajaxNextPagination;
					}
					if (next.length){
						$('div.load').slideDown();
						$.get( next, function( data ) {
							$('div.load').fadeOut( function(){
								var articulos = data.results;
								ajaxNextPagination = data.next;
								var TemplateFinalScript = $("#articulo-template").html();
								var TemplateFinal = Handlebars.compile(TemplateFinalScript);
								var context = articulos;
								var CompiledFinalHtml = TemplateFinal(context);
								$('div.posts .posts-handle').append(CompiledFinalHtml);	
								currently = false;
							});
						});
					}
				}
			} else {
				if(!currently && $(document).height() - win.height() == win.scrollTop() && (ajaxNextPagination == 0 || ajaxNextPagination != null) ) {
					currently = true;if (ajaxNextPagination == 0 && typeof(page) !== 'undefined') {
						next = "/chequeados/api/chequeados/?format=json&page="+page;
					} else {
						next = "";
					}
					if (ajaxNextPagination != 0 && ajaxNextPagination != null ) {
						next = ajaxNextPagination;
					}
					if (next.length){
						$('div.load').slideDown();
						$.get( next, function( data ) {
							$('div.load').fadeOut( function(){
								var articulos = data.results;
								ajaxNextPagination = data.next;
								var TemplateFinalScript = $("#articulo-template").html();
								var TemplateFinal = Handlebars.compile(TemplateFinalScript);
								var context = articulos;
								var CompiledFinalHtml = TemplateFinal(context);
								$('div.posts .posts-handle').append(CompiledFinalHtml);	
								currently = false;
							});
						});
					}
				}
			}
		});
	});
	enquire.register("screen and (min-width: 64em)", function() {
		var currently = false;
		$('.posts').on('scroll', function() {
			if (typeof(cat_principal) !== 'undefined'){
				if(!currently && $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && (ajaxNextPagination == 0 || ajaxNextPagination != null) ) {
					currently = true;
					if (ajaxNextPagination == 0 && typeof(page) !== 'undefined') {
						next = "/chequeados/api/categoria/"+cat_principal+"/?format=json&limit=4&page="+page;
					} else {
						next = "";
					}
					if (ajaxNextPagination != 0 && ajaxNextPagination != null ) {
						next = ajaxNextPagination;
					}
					if (next.length){
						$('div.load').slideDown();
						$.get( next, function( data ) {
							$('div.load').fadeOut( function(){
								var articulos = data.results;
								ajaxNextPagination = data.next;
								var TemplateFinalScript = $("#articulo-template").html();
								var TemplateFinal = Handlebars.compile(TemplateFinalScript);
								var context = articulos;
								var CompiledFinalHtml = TemplateFinal(context);
								$('div.posts .posts-handle').append(CompiledFinalHtml);	
								currently = false;
							});
						});
					}
				}
			} else {
				if(!currently && $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && (ajaxNextPagination == 0 || ajaxNextPagination != null) ) {
					currently = true;
					if (ajaxNextPagination == 0 && typeof(page) !== 'undefined') {
						next = "/chequeados/api/chequeados/?format=json&page="+page;
					} else {
						next = "";
					}
					if (ajaxNextPagination != 0 && ajaxNextPagination != null ) {
						next = ajaxNextPagination;
					}
					if (next.length){
						$('div.load').slideDown();
						$.get( "/chequeados/api/chequeados/?format=json&page=2", function( data ) {
							$('div.load').fadeOut( function(){
								var articulos = data.results;
								ajaxNextPagination = data.next;
								var TemplateFinalScript = $("#articulo-template").html();
								var TemplateFinal = Handlebars.compile(TemplateFinalScript);
								var context = articulos;
								var CompiledFinalHtml = TemplateFinal(context);
								$('div.posts .posts-handle').append(CompiledFinalHtml);	
								currently = false;
							});
						});
					}
				}
			}
		});
	});

	if ($('.cuerpo_articulo').length) {
		var texto = $('.cuerpo_articulo').text();
		var palabras = texto.split(' ').length;
		var wpm = 200;
		var tiempo_lectura = Math.ceil(palabras / wpm);
		$('.tiempo_lectura').text(tiempo_lectura);
	}
	$('#btn_categories').click(function(){
		$(this).toggleClass('current','');
		$('#btn_all').toggleClass('current','');
		$('.posts-handle').fadeToggle( function(){
			$('.categories-handle').fadeToggle();
		});
	});

	$('#btn_all').click(function(){
		$(this).toggleClass('current','');
		$('#btn_categories').toggleClass('current','');
		$('.categories-handle').fadeToggle( function(){
			$('.posts-handle').fadeToggle();
		});
	});
});
